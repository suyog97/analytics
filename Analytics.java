
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Suyog Surana
 */
public class Analytics {
    public void removeDuplicates(String file,String output) throws FileNotFoundException, IOException{
        Set<String> hashset = new HashSet<String>(1000000);
        BufferedReader reader = new BufferedReader(new FileReader(file));
        BufferedWriter writer = new BufferedWriter(new FileWriter(output));
        String line;
        while ((line = reader.readLine()) != null) {
            if(!hashset.contains(line)){
                hashset.add(line);
                writer.write(line);
                writer.newLine();
            }
        }
        reader.close();
        writer.close();
        
    }
    public static void main(String args[]) throws IOException{
        String file = "E:\\Documents\\NetBeansProjects\\Analytics\\src\\file.csv";
        String output = "E:\\Documents\\NetBeansProjects\\Analytics\\src\\Output.csv";
        Analytics A = new Analytics();
        A.removeDuplicates(file, output);
    }
}
